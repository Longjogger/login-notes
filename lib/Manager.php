<?php

declare(strict_types=1);
/**
 * @copyright Copyright (c) 2020 Thomas Citharel <nextcloud@tcit.fr>
 *
 * @author Thomas Citharel <nextcloud@tcit.fr>
 *
 * @license GNU AGPL version 3 or any later version
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

namespace OCA\LoginNotes;

use League\CommonMark\CommonMarkConverter;
use OCA\LoginNotes\Model\Note;
use OCA\LoginNotes\Model\NoteMapper;
use OCP\AppFramework\Db\DoesNotExistException;
use OCP\AppFramework\Db\Entity;
use OCP\AppFramework\Db\MultipleObjectsReturnedException;

class Manager {

	/** @var NoteMapper */
	private $noteMapper;

	/** @var CommonMarkConverter */
	private $converter;

	public function __construct(NoteMapper $noteMapper) {
		$this->noteMapper = $noteMapper;
		$this->converter = new CommonMarkConverter();
	}

	/**
	 * @param int $id
	 * @return Note
	 * @throws DoesNotExistException
	 * @throws MultipleObjectsReturnedException
	 */
	public function getById(int $id): Note {
		return $this->noteMapper->getById($id);
	}

	/**
	 * @param string $text
	 * @return Note|Entity
	 */
	public function create(string $text): Note {
		$text = trim($text);
		$note = new Note();
		$note->setRawText($text);
		$note->setText($this->converter->convertToHtml($text));
		$note->setCreatedAt(time());
		$note = $this->noteMapper->insert($note);
		return $note;
	}

	/**
	 * @param int $noteId
	 * @param string $text
	 * @return Note
	 * @throws DoesNotExistException
	 * @throws MultipleObjectsReturnedException
	 */
	public function update(int $noteId, string $text): Note {
		$text = trim($text);
		$note = $this->noteMapper->getById($noteId);
		$note->setRawText($text);
		$note->setText($this->converter->convertToHtml($text));
		$this->noteMapper->update($note);
		return $note;
	}

	/**
	 * @param Note $note
	 * @return Entity
	 */
	public function delete($note) {
		return $this->noteMapper->delete($note);
	}

	/**
	 * @param null $limit
	 * @param null $offset
	 * @return array
	 */
	public function getNotes($limit = null, $offset = null): array {
		return $this->noteMapper->getNotes($limit, $offset);
	}
}
