/**
 * @copyright Copyright (c) 2020 Thomas Citharel <nextcloud@tcit.fr>
 *
 * @author Thomas Citharel <nextcloud@tcit.fr>
 *
 * @license GNU AGPL version 3 or any later version
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
import { loadState } from '@nextcloud/initial-state'
const notes = loadState('login_notes', 'notes')
const centered = loadState('login_notes', 'centered') || false
// insert warning
const submit = document.querySelector('.wrapper main')
notes.forEach(note => {
	const warning = `<div class="login-notes warning" dir="auto">${note.text}</div>`
	submit.insertAdjacentHTML('afterend', warning)
	const loginNotes = document.querySelector('.wrapper .login-notes.warning')
	loginNotes.style.textAlign = 'start'
	if (centered) {
		loginNotes.style.textAlign = 'center'
	}
})
